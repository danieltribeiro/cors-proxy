'use strict'
const express = require('express');
const request = require('request');

const app = express();

const forward_url = process.env.FORWARD_URL || '*'

const allowedMethods = 'GET, PUT, POST, DELETE, HEAD, PATCH, OPTIONS' // os metodos permitidos
const allowedHeaders = 'Authorization,Content-Type,Accept,X-Amz-Date,X-Api-Key,X-Amz-Security-Token' // os headers permitidos
// const allowedHeaders = 'Accept, Accept-CH, Accept-Charset, Accept-Datetime, Accept-Encoding, Accept-Ext, Accept-Features, Accept-Language, Accept-Params, Accept-Ranges, Access-Control-Allow-Credentials, Access-Control-Allow-Headers, Access-Control-Allow-Methods, Access-Control-Allow-Origin, Access-Control-Expose-Headers, Access-Control-Max-Age, Access-Control-Request-Headers, Access-Control-Request-Method, Age, Allow, Alternates, Authentication-Info, Authorization, C-Ext, C-Man, C-Opt, C-PEP, C-PEP-Info, CONNECT, Cache-Control, Compliance, Connection, Content-Base, Content-Disposition, Content-Encoding, Content-ID, Content-Language, Content-Length, Content-Location, Content-MD5, Content-Range, Content-Script-Type, Content-Security-Policy, Content-Style-Type, Content-Transfer-Encoding, Content-Type, Content-Version, Cookie, Cost, DAV, DELETE, DNT, DPR, Date, Default-Style, Delta-Base, Depth, Derived-From, Destination, Differential-ID, Digest, ETag, Expect, Expires, Ext, From, GET, GetProfile, HEAD, HTTP-date, Host, IM, If, If-Match, If-Modified-Since, If-None-Match, If-Range, If-Unmodified-Since, Keep-Alive, Label, Last-Event-ID, Last-Modified, Link, Location, Lock-Token, MIME-Version, Man, Max-Forwards, Media-Range, Message-ID, Meter, Negotiate, Non-Compliance, OPTION, OPTIONS, OWS, Opt, Optional, Ordering-Type, Origin, Overwrite, P3P, PEP, PICS-Label, POST, PUT, Pep-Info, Permanent, Position, Pragma, ProfileObject, Protocol, Protocol-Query, Protocol-Request, Proxy-Authenticate, Proxy-Authentication-Info, Proxy-Authorization, Proxy-Features, Proxy-Instruction, Public, RWS, Range, Referer, Refresh, Resolution-Hint, Resolver-Location, Retry-After, Safe, Sec-Websocket-Extensions, Sec-Websocket-Key, Sec-Websocket-Origin, Sec-Websocket-Protocol, Sec-Websocket-Version, Security-Scheme, Server, Set-Cookie, Set-Cookie2, SetProfile, SoapAction, Status, Status-URI, Strict-Transport-Security, SubOK, Subst, Surrogate-Capability, Surrogate-Control, TCN, TE, TRACE, Timeout, Title, Trailer, Transfer-Encoding, UA-Color, UA-Media, UA-Pixels, UA-Resolution, UA-Windowpixels, URI, Upgrade, User-Agent, Variant-Vary, Vary, Version, Via, Viewport-Width, WWW-Authenticate, Want-Digest, Warning, Width, X-Content-Duration, X-Content-Security-Policy, X-Content-Type-Options, X-CustomHeader, X-DNSPrefetch-Control, X-Forwarded-For, X-Forwarded-Port, X-Forwarded-Proto, X-Frame-Options, X-Modified, X-OTHER, X-PING, X-PINGOTHER, X-Powered-By, X-Requested-With'

function addCors(req, res) {
    res.header('Access-Control-Allow-Origin', req.headers.origin || '*')
    res.header('Access-Control-Allow-Methods', allowedMethods)
    res.header('Access-Control-Allow-Headers', allowedHeaders)
    res.header('Access-Control-Allow-Credentials', true)
}

app.use('/health', function(req, res, next) {
    res.send({STATUS: "UP"})
})

app.use('/*', function(req, res, next) {


    var path = req.originalUrl
    
    var url = path.substring(1)
    if (forward_url != '*') {
        url = forward_url + path
    }

    if (req.method == 'OPTIONS') {
        addCors(req, res)
        res.send()
        return
    }

    var newHeaders = {};
    for (var i in req.headers) {
        if (i != 'host') {
            newHeaders[i] = req.headers[i];
        }
    }

    newHeaders.origin = url.origin;

    var options = {
      url: url,
      headers: newHeaders,
      method: req.method
    }
    console.log(`forward to ${options.url}`)
    var x = request(options)
    req.pipe(x)
    addCors(req, res)
    x.pipe(res)
  });

const PORT = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080;
const IP = process.env.NODEJS_IP || process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1'

app.listen(PORT, () => console.log(`listening on ${IP}:${PORT}, with CORS`));

module.exports = app